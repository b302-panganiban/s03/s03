/* [C]reate*/

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

/* INSERT INTO tableName (columnA, columnB, columnC) VALUES (columnAValue, columnBValue, columnCValue);*/
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Psy 6", "2021-1-1", 2);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Trip", "1991-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 230, "OPM", 2);

/* [R]etrieve */
/* Retrieve all column values of songs table */
SELECT * FROM songs;

/* Display the song_name and genre of all the songs */
SELECT song_name, genre FROM songs;

SELECT song_name, album_id FROM songs;


-- Display the song_name of all the OPM songs
-- [WHERE]
SELECT song_name, genre from songs WHERE genre ="OPM";

/* [U]pdate */
/* UPDATE tableName SET column = value WHERE column = value */
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

/* [D]elete */
DELETE FROM songs WHERE genre = "OPM" AND length > 200;

/* AND */
-- All must be True for the result to be True

/* OR */
-- One true will result to True

